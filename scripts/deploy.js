async function main() {
    // We get the contract to deploy
    const EventManager = await ethers.getContractFactory("EventManager");
    const eventManager = await EventManager.deploy();
  
    console.log("EventManager deployed to:", eventManager.address);
}
  
main().then(() => process.exit(0)).catch(error => {
    console.error(error);
    process.exit(1);
});
  